![Texto alternativo](http://institucional.pedagogica.edu.co/proyectos/admin/agendapedagogica/docs/notas/azul_horizontal_logo_upn.png "Título alternativo")

    :Author: Jahir Lopez
             Oscar Mendez
    :Date:   2018/12/10 
    :Maestria en tecnologias de la información aplicadas a la educación        



## Tecnicas de machine learning para la simulacion y deteccion de dificultades en el desarrollo de la dimensión proyectiva del pensamiento espacial
### Contenido del repositorio, carpeta **ANEXOS**:
En esta carpeta se guardan los notebook finales mas el .sav del modelo
- ANEXO 2: exploracion de datos
- ANEXO 3: preparacion de datos y simulacion
- ANEXO 4: seleccion de atributos
- ANEXO 5: modelo final
- ANEXO 6: uso del modelo final para prediccion


> Carpeta **experimentos** se encuentran notebooks con pruebas de librerias, simulaciones, graficos, algoritmos etc


> Carpeta **borradores** estan los notebooks usados durante el desarrollo y con los que se generaron los finales que se encuentran en **ANEXOS**

> Carpeta **data** contiene los dataset utilizados en el desarrollo del modelo